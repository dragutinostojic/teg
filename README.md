# TEG

This repository is a collection of data I collected towards the Erasmus+ research mobility programme at Silesian University of Technology with Prof. Tomasz Golonek.

Measurements were taken for the output current of system $I_{TEGk}$, output voltage of system $V_{TEGk}$, frequency of generated PWM pulse ($f_{PWMk}$), and 3 additionally voltage values in system on experimentally selected nodes ($V_{1k}$, $V_{2k}$ and $V_{3k}$).

10 rounds of measurements were done, everyone with $10^5$ tuples of samples, and one by one with values of frequency $f_{PWMk}$: 0 Hz, 128 Hz, 256 Hz, 512 Hz, 1024 Hz, 2048 Hz, 4096 Hz, 8192 Hz, 12000 Hz and 15000 Hz. Other parameters defining operational point of the system are manually and indirectly changed during the acquisition process by changing values of input power and consumer resistance. 

Generated data has a lot of noise, and raw data is not acceptable for training predicting models in this form. First attempt was made to neutralize the noise with grouping data in time windows, each window with 10, 100 and 1000 samples. In the first method of preprocesing all data in one window is replaced with their average value for each column, and in the second method, the same principle with median for every column was used.


On this data various regression models are trained with goal to determine $I_{TEGk}$ when other parameters are known. Next figure represents one of trees of CatBoostRegressor model.

<img src="/tree8.png" width="900" />
